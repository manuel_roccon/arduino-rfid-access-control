//Author:Manuel Roccon manuel.roccon@gmail.com
//Arduino RFID control access

#include <SPI.h>
#include <RFID.h>
//#include <LiquidCrystal.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define SS_PIN 10
#define RST_PIN 9

#define relayPin 8

RFID rfid(SS_PIN, RST_PIN);
// initialize the library with the numbers of the interface pins
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

const int keyPin = 2; //the number of the key pin
// Setup variables:
int serNum0 = 0;
int serNum1 = 0;
int serNum2 = 0;
int serNum3 = 0;
int serNum4 = 0;
int buzzer = 9;//the pin of the active buzzer
int i;
const int redPin = 5;  // R petal on RGB LED module connected to digital pin 11 
const int greenPin = 6;  // G petal on RGB LED module connected to digital pin 9 
const int bluePin = 7;  // B petal on RGB LED module connected to digital pin 10 

void setup()
{ 
    pinMode(buzzer,OUTPUT);//initialize the buzzer pin as an output
     pinMode(keyPin,INPUT);//initialize the key pin as input 
    lcd.init();  //initialize the lcd
    lcd.backlight();  //open the backlight  
    lcd.print(" Access Control "); 
   
    Serial.begin(9600);
    pinMode(relayPin, OUTPUT);
    SPI.begin(); 
    rfid.init();
        delay(10);
        buzzerTone(3, 30);
        delay(10);
        buzzerTone(2, 30);
        delay(10);
        buzzerTone(1, 30);
    delay(2000);
    buzzerTone(1, 30);
}

void loop()
{ 
  if(digitalRead(keyPin) ==HIGH )
  {
      buzzerTone(1, 30);
      lcd.setCursor(0,0);
      lcd.print("  PROGRAMMING   "); 
      lcd.setCursor(0,1);
      lcd.print("   PASS CARD    ");
      delay(1000);
      int stoploop = false;
      while (digitalRead(keyPin)== LOW && !stoploop){
          if (rfid.isCard()) 
          {
            if (rfid.readCardSerial()) 
            {
              serNum0 = rfid.serNum[0];
              serNum1 = rfid.serNum[1];
              serNum2 = rfid.serNum[2];
              serNum3 = rfid.serNum[3];
              serNum4 = rfid.serNum[4];
              lcd.setCursor(0,1);
              lcd.print("   CARD SAVED!  ");  
              color(255,255, 255); // turn the RGB LED green  
              buzzerTone(1, 80);
              delay(10);
              buzzerTone(1, 80);
              delay(2000);
              stoploop = true;
            }
          }
      }
      buzzerTone(1, 30);
      lcd.setCursor(0,0);
      lcd.print(" Access Control "); 
      
      delay(500);
  }
  if (rfid.isCard()) 
  {
    if (rfid.readCardSerial()) 
    {
      if(rfid.serNum[0] == serNum0 && rfid.serNum[1] == serNum1 && rfid.serNum[2] == serNum2 && rfid.serNum[3] == serNum3 && rfid.serNum[4] == serNum4)
      {
        lcd.setCursor(0,1);
        lcd.print("  ACCESS GRANT  ");      
        digitalWrite(relayPin, HIGH);
        color(0,255, 0); // turn the RGB LED green  
        buzzerTone(1, 80);
        delay(10);
        buzzerTone(1, 80);
      }
      else 
      {
        /* If we have the same ID, just write a dot. */
        lcd.setCursor(0,1);
        lcd.print("                 ");
        lcd.setCursor(0,1);
        lcd.print(" ACCESS DENIED  ");
        color(255, 0, 0); // turn the RGB LED red 
        buzzerTone(2, 80);
        delay(10);
        buzzerTone(3, 80);
        digitalWrite(relayPin, LOW);
      }
      delay(2000);
      digitalWrite(relayPin, LOW);
      rfid.halt();
      while (rfid.isCard()==true){
        lcd.setCursor(0,1);
        lcd.print("  REMOVE CARD  ");
        rfid.halt();
      } 
    }
  }
  color(0, 0, 0); // turn the RGB LED red 
  lcd.setCursor(0,1);
  lcd.print("     READY      ");
  digitalWrite(relayPin, LOW);
  rfid.halt();
}

/******************************************************/     
void buzzerTone(int t, int d){
  for(i=0;i<d;i++)
      {
          digitalWrite(buzzer,HIGH);
          delay(t);//wait for 2ms
          digitalWrite(buzzer,LOW);
          delay(t);//wait for 2ms
      }
}
void color (unsigned char red, unsigned char green, unsigned char blue)     // the color generating function  
{    
          analogWrite(redPin, red);   
          analogWrite(bluePin, blue); 
          analogWrite(greenPin, green); 
}
/******************************************************/
